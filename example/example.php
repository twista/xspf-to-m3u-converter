<?php

require __DIR__.'/../src/converter.php';

$pl = 'playlist.xspf';

$converter = Twista\XSPFConverter::fromFile($pl);

file_put_contents('playlist.m3u', $converter->toM3u());